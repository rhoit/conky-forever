#!/usr/bin/env python3

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk
from gi.repository import GObject

import os
import fcntl
import subprocess

def unblock_fd(stream):
    fd = stream.fileno()
    fl = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)


class StreamTextBuffer(Gtk.TextBuffer):
    '''TextBuffer read command output syncronously'''
    def __init__(self):
        Gtk.TextBuffer.__init__(self)
        self.IO_WATCH_ID = tuple()


    def bind_subprocess(self, proc):
        unblock_fd(proc.stdout)
        watch_id_stdout = GObject.io_add_watch(
            channel   = proc.stdout,
            priority_ = GObject.IO_IN,
            condition = self.buffer_update,
            # func      = lambda *a: print("func") # when the condition is satisfied
            # user_data = # user data to pass to func
        )

        unblock_fd(proc.stderr)
        watch_id_stderr = GObject.io_add_watch(
            channel   = proc.stderr,
            priority_ = GObject.IO_IN,
            condition = self.buffer_update,
            # func      = lambda *a: print("func") # when the condition is satisfied
            # user_data = # user data to pass to func
        )

        self.IO_WATCH_ID = (watch_id_stdout, watch_id_stderr)
        return self.IO_WATCH_ID


    def buffer_update(self, stream, condition):
        self.insert_at_cursor(stream.read())
        return True # otherwise isn't recalled


def sample():
    root = Gtk.Window()
    root.set_default_size(400, 260)
    root.connect("destroy", Gtk.main_quit)
    root.connect( # quit when Esc is pressed
        'key_release_event',
        lambda w, e: Gtk.main_quit() if e.keyval == 65307 else None
    )
    layout = Gtk.Box(orientation=1)
    scroll = Gtk.ScrolledWindow()
    layout.pack_start(scroll, expand=1, fill=1, padding=0)

    buff = StreamTextBuffer()
    textview = Gtk.TextView.new_with_buffer(buff)
    scroll.add(textview)

    button_start = Gtk.Button("Execute Command")
    layout.add(button_start)

    def on_click(widget):
        if len(buff.IO_WATCH_ID):
            for id_ in buff.IO_WATCH_ID:
                # remove subprocess io_watch if not removed will
                # creates lots of cpu cycles, when process dies
                GObject.source_remove(id_)
            buff.IO_WATCH_ID = tuple()
            on_click.proc.terminate() # send SIGTERM
            widget.set_label("Execute Command")
            return

        on_click.proc = subprocess.Popen(
            [ 'ping', '-c', '3', 'localhost' ],
            stdout = subprocess.PIPE,
            stderr = subprocess.PIPE,
            universal_newlines=True,
        )
        buff.bind_subprocess(on_click.proc)
        widget.set_label("STOP!")

    button_start.connect("clicked", on_click)
    root.add(layout)
    root.show_all()


if __name__ == "__main__":
    sample()
    Gtk.main()
