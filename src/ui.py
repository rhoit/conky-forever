#!/usr/bin/env python3

import os, sys

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import GObject, GLib
from gi.repository import Gtk, Gdk
from gi.repository import Pango

import re
import subprocess

import ui_fdbuffer
import ui_notebook_pages


class PStream(ui_fdbuffer.StreamTextBuffer):
    def __init__(self):
        ui_fdbuffer.StreamTextBuffer.__init__(self)
        self.WINDOW_ID = ""
        self.proc = None
        self.re_winid = re.compile(
            "Conky: drawing to created window \((.*)\)",
            re.DOTALL
        )


    def create_subprocess(self, cmd, **kwargs):
        self.proc = subprocess.Popen(
            cmd,
            stdout = subprocess.PIPE,
            stderr = subprocess.PIPE,
            universal_newlines=True,
            **kwargs
        )
        self.bind_subprocess(self.proc)


    def terminate(self):
        if self.proc is None: return
        self.insert_at_cursor("STOPPED..\n\n")
        for id_ in self.IO_WATCH_ID:
            GObject.source_remove(id_)

        self.proc.terminate()
        del self.proc
        self.proc = None
        return True


    def buffer_update(self, stream, condition):
        text = stream.read()
        match = self.re_winid.match(text)
        if match:
            self.WINDOW_ID = match.group(1)
            print(self.WINDOW_ID)
        self.insert_at_cursor(text)
        return True # otherwise isn't recalled


class Home(Gtk.Window):
    def __init__(self, cnf):
        Gtk.Window.__init__(self, name="Home")
        self.cnf = cnf
        self.buffer_ps = dict()
        self.CURRENT = 0

        self.makeWidgets()
        self.loadCSS()

        self.connect('key_press_event', self.on_key_press)
        self.connect('key_release_event', self.on_key_release)

        self.set_default_size(800, 600)
        self.set_position(Gtk.WindowPosition.CENTER)

        for c, (key, obj) in enumerate(cnf.widget_list.items()):
            buff = PStream()
            self.buffer_ps[key] = buff
            icon_name = ""
            if obj['start']:
                cmd = [ 'conky', '-c', key + '/main.rc' ]
                buff.create_subprocess(cmd, cwd=key)
                icon_name = 'gtk-yes'

            self.listed.treemodel.append(
                [key, obj['name'], icon_name]
            )

        treeSelection = self.listed.treeview.get_selection()
        treeSelection.select_path(0)


    def makeWidgets(self):
        layout = Gtk.Box(orientation=1)
        self.add(layout)

        layout.add(self.makeWidget_toolbar())

        hpaned = Gtk.Paned(name="hpaned")
        layout.pack_start(hpaned, expand=1, fill=1, padding=0)

        hpaned.pack1(self.makeWidget_sidebar(), resize=0, shrink=0)
        hpaned.pack2(self.makeWidget_notebook(), resize=0, shrink=0)
        hpaned.set_position(250)

        layout.show_all()


    def loadCSS(self):
        self.css_provider = Gtk.CssProvider()
        css = __file__.replace('.py', '.css')
        if not os.path.exists(css): return
        self.css_provider.load_from_path(css)
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            self.css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )


    def makeWidget_toolbar(self):
        self.toolbar = Gtk.Toolbar()
        return self.toolbar


    def makeWidget_sidebar(self):
        layout = Gtk.Box(orientation=1)

        stack = Gtk.Stack()
        stack_switcher = Gtk.StackSwitcher()
        stack_switcher.set_stack(stack)

        layout.pack_start(stack_switcher, expand=0, fill=1, padding=0)
        layout.pack_start(stack, expand=1, fill=1, padding=0)

        self.listed = self.makeWidget_treeview()
        stack.add_titled(self.listed, 'listed', 'Listed')

        stack.add_titled(Gtk.Label("Not Implimented"), 'scan', 'Scan')

        return layout


    def makeWidget_treeview(self):
        scroll = Gtk.ScrolledWindow()

        scroll.set_hexpand(True)
        scroll.set_vexpand(True)

        scroll.treemodel = Gtk.ListStore(str, str, str)

        scroll.treeview = Gtk.TreeView(scroll.treemodel)
        scroll.add(scroll.treeview)
        scroll.treeview.set_search_column(1)

        c1 = Gtk.TreeViewColumn("", Gtk.CellRendererPixbuf(), icon_name=2)
        scroll.treeview.append_column(c1)

        c2 = Gtk.TreeViewColumn("name", Gtk.CellRendererText(), text=1)
        c2.set_expand(True)
        scroll.treeview.append_column(c2)

        # binding
        treeSelection = scroll.treeview.get_selection()
        treeSelection.connect(
            "changed",
            self.sidebar_on_row_changed
        )
        return scroll


    def sidebar_on_row_changed(self, treeSelection):
        model, pathlist = treeSelection.get_selected_rows()
        for index in pathlist:
            path, name, iconname = model[index]
            self.CURRENT = path
            self.enter_data(path)


    def makeWidget_notebook(self):
        self.notebook = Gtk.Notebook()

        self.notebook.set_hexpand(True)
        self.notebook.set_vexpand(True)
        self.notebook.set_scrollable(True)

        self.page_info = ui_notebook_pages.Page_info()
        self.notebook.append_page(self.page_info, Gtk.Label("Info"))


        def on_toggle_signal(w):
            treeSelection = self.listed.treeview.get_selection()
            model, pathlist = treeSelection.get_selected_rows()
            index = pathlist[0]
            path, name, iconname = model[index]

            if w.get_active():
                self.run(self.CURRENT)
                model[index][2] = 'gtk-yes'
            else:
                self.stop(self.CURRENT)
                model[index][2] = ''

        self.page_info.b_PLAY.connect("toggled", on_toggle_signal)
        return self.notebook


    def stop(self, path):
        buff = self.buffer_ps.get(path)
        if buff is None:
            print("error :", path, "no process found")
            return
        if buff.proc is None:
            print("alert :", path, "not running")
            return

        buff.terminate()


    def run(self, path):
        buff = self.buffer_ps.get(path, PStream())
        if buff.proc is not None :
            print("alert :", path, "already running")
            return

        cmd = [ 'conky', '-DD', '-c', path + '/main.rc' ]
        buff.create_subprocess(cmd, cwd=path)
        self.buffer_ps[path] = buff


    def enter_data(self, path):
        widget = self.cnf.widget_list[path]
        self.page_info.title.set_label(widget['name'])
        self.page_info.desc.set_markup("<i>%s</i>"%widget['desc'])
        self.page_info.about.set_label(widget['about'])
        self.page_info.authors.set_markup("<b>Authors:</b> " + widget['authors'])
        self.page_info.img.set_from_file(path + '/screenshot.png')

        buff = self.buffer_ps[path]
        self.page_info.ctv.set_buffer(buff)
        self.page_info.b_PLAY.set_active(buff.proc is not None)


    def on_key_press(self, widget, event):
        # NOTE to stop propagation of signal return True
        if event.keyval == 65307: return self.hide() # Esc


    def on_key_release(self, widget, event):
        if   event.keyval == 65474: self.core.Glossary.reload() # F5
        elif Gdk.ModifierType.CONTROL_MASK & event.state:
            if   event.keyval == ord('q'): self.emit("delete-event", Gdk.Event.new(Gdk.EventType.DELETE))
            elif event.keyval == ord('Q'): self.emit("delete-event", Gdk.Event.new(Gdk.EventType.DELETE))


def main(core, cnf):
    win = Home(core, cnf)
    win.show()
    return win


def sample():
    pwd = sys.path[0]+'/../'
    sys.path.append(pwd)
    import setting
    cnf = setting.main(pwd)

    root = main(core, cnf)
    root.connect('delete-event', Gtk.main_quit)

    # in isolation testing, make Esc quit Gtk mainloop
    root.handle_esc = Gtk.main_quit


if __name__ == '__main__':
    sample()
    Gtk.main()
