#!/usr/bin/env python3

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Pango

class Page_info(Gtk.Grid):
    def __init__(self):
        Gtk.Grid.__init__(
            self,
            name="info",
            orientation=1
        )
        self.makeWidgets()


    def makeWidgets(self):
        self.attach(self.makeWidget_meta(), left=0, top=0, width=4, height=4)

        self.img = Gtk.Image(name="screenshot")
        self.img.set_hexpand(False)
        self.img.set_vexpand(False)
        self.attach(self.img, left=5, top=0, width=1, height=4)

        self.makeWidget_controls()
        self.makeWidget_console()

    def makeWidget_meta(self):
        meta = Gtk.Box(name="meta", orientation=1)
        meta.set_hexpand(True)
        meta.set_vexpand(False)

        self.title = Gtk.Label(name="title")
        meta.pack_start(self.title, expand=0, fill=0, padding=0)
        self.title.set_alignment(xalign=0, yalign=0.5)

        self.desc = Gtk.Label(name="desc")
        meta.pack_start(self.desc, expand=0, fill=0, padding=0)
        self.desc.set_alignment(xalign=0, yalign=0.5)

        self.about = Gtk.Label(name="about")
        meta.pack_start(self.about, expand=0, fill=0, padding=0)
        self.about.set_alignment(xalign=0, yalign=0.5)
        self.about.set_line_wrap(True)
        self.about.set_line_wrap_mode(Pango.WrapMode.WORD)

        self.authors = Gtk.Label(name="authors")
        meta.pack_start(self.authors, expand=0, fill=0, padding=0)
        self.authors.set_alignment(xalign=0, yalign=0.5)

        return meta


    def makeWidget_controls(self):
        wrap_box = Gtk.Box() # HACK: to avoid streach
        self.b_PLAY = Gtk.ToggleToolButton(icon_name="media-playback-start")
        wrap_box.pack_start(self.b_PLAY, expand=0, fill=0, padding=0)
        self.b_PLAY.connect(
            "toggled",
            lambda w: (
                w.set_icon_name("process-stop")
                if w.get_active() else
                w.set_icon_name("media-playback-start")
            )
        )

        self.attach(wrap_box, left=0, top=6, width=1, height=1)


    def makeWidget_console(self):
        exp = Gtk.Expander.new("Details")
        self.attach(exp, left=0, top=8, width=6, height=2)
        exp.connect(
            "activate",
            lambda w: w.set_vexpand(not w.get_expanded())
        )
        exp.set_expanded(True)

        scroll = Gtk.ScrolledWindow()
        exp.add(scroll)
        scroll.set_hexpand(True)
        scroll.set_vexpand(True)

        self.ctv = Gtk.TextView.new()
        scroll.add(self.ctv)

        self.ctv.modify_font(Pango.FontDescription("monospace"))
        self.ctv.set_editable(False)
        self.ctv.set_cursor_visible(False)

        self.ctv.set_left_margin(10)
        self.ctv.set_top_margin(10)
        self.ctv.connect(
            "button-press-event",
            lambda *a: True
        )
        return exp
