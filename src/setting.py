#!/usr/bin/env python3
"""Application Settings handler"""

import os
import configparser

PATH_INI   = os.path.expanduser('~/.config/')
PATH_CACHE = os.path.expanduser('~/.cache/conky-forever/')

FILE_INI          = PATH_INI + 'conky-forever.ini'
FILE_DEFAULT_INI  = '../default.ini'

from collections import OrderedDict

class Settings(configparser.ConfigParser):
    __instance__ = None

    def __new__(cls, *args, **kargs):
        """Override the __new__ method to make singleton"""
        if cls.__instance__ is None:
            cls.__instance__ = configparser.ConfigParser.__new__(cls)
            cls.__instance__.singleton_init(*args, **kargs)
        return cls.__instance__


    def __init__(self):
        # NOTE: object, aren't properly created for some unknown cause
        # until init was declared.
        pass


    def singleton_init(self):
        configparser.ConfigParser.__init__(self)
        self.widget_list = OrderedDict()
        self.preferences, self['preferences'] = None, dict()


    def read(self, conf_file):
        path = os.path.expanduser(conf_file)
        if not os.path.isfile(path): return
        configparser.ConfigParser.read(self, path)


    def load(self):
        self.preferences = self.extract_preferences()
        for section in self.sections():
            if 'conky' in section:
                id_ = section.split('"')[1]
                path, obj = self.new_widget(id_, self[section])
                self.widget_list[path] = obj


    def new_widget(self, id_, obj=None):
        # NOTE: this is just the wrapper for central var control
        ## dict() should send var in its own datatype
        get        = obj.get        if hasattr(obj, 'get') else lambda k, d: d
        getint     = obj.getint     if hasattr(obj, 'getint') else lambda k, d: get(k, d)
        getfloat   = obj.getfloat   if hasattr(obj, 'getfloat') else lambda k, d: get(k, d)
        getboolean = obj.getboolean if hasattr(obj, 'getboolean') else lambda k, d: get(k, d)

        return get('path', PATH_CACHE + id_), {
            'name'    : get('name', id_),
            'desc'    : get('desc', 'short description'),
            'about'   : get('about', 'about widgets'),
            'start'   : getboolean('start', True),
            'authors' : get('authors', 'list of authors'),
        }

    def extract_preferences(self):
        pref = self['preferences']
        return {
            'conkyrc-folder'  : pref.get('conkyrc-folder', PATH_CACHE),
            'hide-on-startup' : pref.getboolean('hide-on-startup', False),
            'scan-on-startup' : pref.getboolean('scan-on-startup', False),
        }


    def apply_args_request(self, opts):
        self.preferences['hide-on-startup'] = not opts.show
        self.preferences['scan-on-startup'] = opts.scan


def main(pwd=""):
    cnf = Settings()
    global FILE_DEFAULT_INI
    FILE_DEFAULT_INI = pwd + FILE_DEFAULT_INI
    cnf.read(FILE_DEFAULT_INI)
    cnf.read(FILE_INI)
    cnf.load()
    return cnf


if __name__ == '__main__':
    cnf = main()
    from pprint import pprint
    pprint(cnf.preferences)
    pprint(cnf.glossary_list)
