#!/usr/bin/python3
#^^^ not using env for process name

__version__  = 0.1
__PKG_ID__   = "apps.conky-forever"
__PKG_NAME__ = "conky-forever"
__PKG_DESC__ = "Desktop Widget Manager"

import os, sys

__filepath__ = os.path.realpath(__file__)
PWD = os.path.dirname(__filepath__) + '/'

# sys.path.append(PWD)

import argparse
import signal

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gdk
from gi.repository import GLib, Gio

import setting
import ui

class TrayIcon(Gtk.StatusIcon):
    def __init__(self, app, visible=True):
        Gtk.StatusIcon.__init__(self)
        self.app = app
        self.visible = visible
        self.makeWidget()
        self.connect("activate", self.toggle_visibility)
        self.connect("popup_menu", self.on_secondary_click)


    def makeWidget(self):
        self.set_from_pixbuf(self.app.pixbuf_logo)
        self.set_title(__PKG_NAME__)
        # self.set_name(__PKG_ID__ + ".tray")
        ## BUG: don't use it ^^^ will create Gdk-CRITICAL
        self.set_tooltip_text(__PKG_DESC__)
        self.set_has_tooltip(True)
        self.set_visible(True)

        self.menu = Gtk.Menu()

        menuitem_visibility = Gtk.MenuItem("Show / Hide")
        menuitem_visibility.connect("activate", self.toggle_visibility)
        self.menu.append(menuitem_visibility)

        menuitem_quit = Gtk.MenuItem("Quit")
        menuitem_quit.connect("activate", lambda *a: self.app.quit())
        self.menu.append(menuitem_quit)
        self.menu.show_all()


    def toggle_visibility(self, widget):
        self.visible = not self.visible
        if self.visible:
            self.app.activate()
            return

        self.app.home.hide()


    def on_secondary_click(self, widget, button, time):
        self.menu.popup(None, None, None, self, 3, time)


class App(Gtk.Application):
    def __init__(self, opts):
        Gtk.Application.__init__(
            self,
            application_id = __PKG_ID__,
        )

        self.opts = opts
        self.home = None
        self.connect("shutdown", __class__._on_shutdown)
        ## BUG: dont use =do_shutdown= virtual func, creates show
        ## CRITIAL error messages


    def do_startup(self):
        Gtk.Application.do_startup(self)

        self.cnf = setting.main(PWD)
        self.cnf.apply_args_request(self.opts)

        self.icon_theme = Gtk.IconTheme.get_default()
        self.pixbuf_logo = self.icon_theme.load_icon(
            'application-x-addon', 128,
            Gtk.IconLookupFlags.FORCE_SVG
        )


    def do_activate(self):
        if self.home == None:
            self.home = create_home_window(self.cnf, self.pixbuf_logo)
            self.add_window(self.home)

            self.tray = TrayIcon(self)
            if self.cnf.preferences['hide-on-startup']: return

        self.home.show()
        self.home.present()


    def _on_shutdown(cls):
        for k, buff in cls.home.buffer_ps.items():
            print("closing:", k, buff.terminate())
            # p.send_signal(signal.SIGKILL)


def create_home_window(cnf, pixbuf_logo):
    home = ui.Home(cnf)
    home.set_icon(pixbuf_logo)
    home.set_title(__PKG_NAME__)
    home.connect('delete-event', confirm_exit)

    return home


def confirm_exit(widget, event):
    dialog = Gtk.MessageDialog(
        transient_for=widget,
        modal=True,
        buttons=Gtk.ButtonsType.OK_CANCEL
    )

    dialog.props.text = 'Are you sure you want to quit?'
    def on_key_release(widget, event):
        if Gdk.ModifierType.CONTROL_MASK & event.state:
            if event.keyval in (ord('q'), ord('Q'),  ord('c'), ord('C')):
                dialog.emit('response', Gtk.ResponseType.OK)

    dialog.connect('key_release_event', on_key_release)

    response = dialog.run()
    dialog.destroy()

    if response == Gtk.ResponseType.OK:
        # BUG: app.quit() coz not quitting when emitted
        widget.destroy()
        return False

    return True


def create_about_dialog(pixbuf, parent=None):
    about = Gtk.AboutDialog(title="about", parent=parent)
    about.set_position(Gtk.WindowPosition.CENTER_ON_PARENT)
    about.set_logo(pixbuf)
    about.set_program_name(__PKG_NAME__)
    about.set_comments("%s\n\nv%s\n"%(__PKG_DESC__, __version__))
    about.set_website("http://github.com/rhoit/conky-forever")
    about.set_authors(open(PWD + '../AUTHORS').read().splitlines())
    about.set_license(open(PWD + '../LICENSE').read())
    about.run()
    about.destroy()


def on_help(home):
    screen = home.get_screen();
    Gtk.show_uri(screen, "help:anubad", Gtk.get_current_event_time());


def create_arg_parser():
    parser = argparse.ArgumentParser(
        prog=__PKG_NAME__,
        description=__PKG_DESC__
    )

    parser.add_argument(
        "--version",
        action  = "version",
        version = '%(prog)s v' + str(__version__))

    parser.add_argument(
        "--show",
        action  = "store_true",
        default = False,
        help    = "Show on startup")


    parser.add_argument(
        "--scan",
        action  = "store_true",
        default = False,
        help    = "Scan for new widgets")


    return parser


def main():
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    parser = create_arg_parser()
    opts, unknown = parser.parse_known_args()
    return App(opts)


if __name__ == '__main__':
    app = main()
    app.run()
