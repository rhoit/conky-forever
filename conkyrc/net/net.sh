#!/bin/bash

netstat -npt 2> /dev/null | awk -v host="$(hostname -i | tr -d ' ')" 'BEGIN { FS = "[/: ]+"} NR > 2 {gsub(host, "localhost"); gsub("127.0.0.1|::1", "localhost"); if ($5 < 10240) printf ":%-5s ← %-15s %s\n", $5, $6, $(NF - 1); }' | sort | uniq -c | sort -rn | cut -b 1-3 --complement
