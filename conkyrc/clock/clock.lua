#!/usr/bin/env lua

require 'cairo'

clock_r = 64
clock_x = 68
clock_y = 68

hh_color = 0xffffff; hh_alpha = 0.3
mm_color = 0xffffff; mm_alpha = 0.6
ss_color = 0xffff99; ss_alpha = 0.3

-- color switch toggle hack
color = { 0x1f16F, 0x91E990, 0x1f16F } -- bg, fg, bg
alpha = { 0.1, 0.9, 0.1 }  -- bg, fg, bg

function get_rgba(rgb, alpha)
    r = ((rgb / 0x10000) % 0x100) / 255.0
    g = ((rgb / 0x100) % 0x100) / 255.0
    b = (rgb % 0x100) / 255.0
    return r, g, b, alpha
end

function draw_ring(cr, xc, yc, r, angle, s)
    local a0 = math.pi * (- 0.5)
    local a1 = math.pi * 1.5
    local as = angle * (2 * math.pi)

    cairo_set_source_rgba(cr, get_rgba(color[s+1], alpha[s+1]))

    if t == 0 then
        cairo_arc(cr, xc, yc, r, a0, a1)
    else
        cairo_arc(cr, xc, yc, r, a0, a0+as)
    end
    cairo_stroke(cr)

    cairo_set_source_rgba(cr, get_rgba(color[s+2], alpha[s+2]))
    cairo_arc(cr, xc, yc, r, a0+as, a1)
    cairo_stroke(cr)
end


function draw_clock_hands(cr, xc, yc)
    ss_arc = (ss/30)*math.pi
    mm_arc = (mm/30)*math.pi + ss_arc/60
    hh_arc = (hh/06)*math.pi + mm_arc/12

    cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND)

    -- hour hand
    xh = xc + 0.6*clock_r*math.sin(hh_arc)
    yh = yc - 0.6*clock_r*math.cos(hh_arc)
    cairo_move_to(cr, xc, yc)
    cairo_line_to(cr, xh, yh)

    cairo_set_line_width(cr, 5)
    cairo_set_source_rgba(cr, get_rgba(hh_color, hh_alpha))
    cairo_stroke(cr)

    -- minute hand
    xm = xc + 0.8*clock_r*math.sin(mm_arc)
    ym = yc - 0.8*clock_r*math.cos(mm_arc)
    cairo_move_to(cr, xc, yc)
    cairo_line_to(cr, xm, ym)

    cairo_set_line_width(cr, 3)
    cairo_set_source_rgba(cr, get_rgba(mm_color,mm_alpha))
    cairo_stroke(cr)

    -- second hand
    xs = xc + clock_r*math.sin(ss_arc)
    ys = yc - clock_r*math.cos(ss_arc)

    xss = xc - 0.2*clock_r*math.sin(ss_arc)
    yss = yc + 0.2*clock_r*math.cos(ss_arc)

    cairo_move_to(cr, xss, yss)
    cairo_line_to(cr, xs, ys)

    cairo_set_line_width(cr, 1)
    cairo_set_source_rgba(cr, get_rgba(ss_color,ss_alpha))
    cairo_stroke(cr)
end


function conky_clock_rings()
    if conky_window == nil then return end
    local cs = cairo_xlib_surface_create(
        conky_window.display,
        conky_window.drawable,
        conky_window.visual,
        conky_window.width,
        conky_window.height
    )

    local cr = cairo_create(cs)

    ss = os.date("%S")
    mm = os.date("%M")
    hh = os.date("%I")

    cairo_set_line_width(cr, 5)
    draw_ring(cr, clock_x, clock_y, clock_r, ss/60, mm%2)

    draw_clock_hands(cr, clock_x, clock_y)
    cairo_surface_destroy(cs)
    cairo_destroy(cr)
end
